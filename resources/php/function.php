<?php

// If an entry in source array is not found in target array, the function will add the
// entry into target array. If an entry already exists in the target array, the function
// will then look for differences in field values and replace the target field value with
// the values from source field value if any is found.
function matchArray($srcArray, $tgtArray, $srcPK, $tgtPK, $tgtHeader, $newSrcHeader) {
    foreach ($srcArray as $srcKey => $srcVal) {
        if (empty($srcVal[$srcPK]) || !in_array($srcVal[$srcPK], array_column($tgtArray, $tgtPK))) {
            $tgtArray[] =  rearrangeArray($srcArray, $srcKey, $tgtHeader, $newSrcHeader);
        }

        foreach ($tgtArray as $tgtKey => $tgtVal) {
            if (!empty($srcVal[$srcPK]) && !empty($tgtVal[$tgtPK]) && $srcVal[$srcPK] == $tgtVal[$tgtPK]) {
                $newSrcEntry = rearrangeArray($srcArray, $srcKey, $tgtHeader, $newSrcHeader);

                foreach ($tgtHeader as $tgtHVal) {
                    if (!empty($newSrcEntry[$tgtHVal]) &&
                        $newSrcEntry[$tgtHVal] != $tgtVal[$tgtHVal]) {

                        $tgtVal[$tgtHVal] = $newSrcEntry[$tgtHVal];
                        $tgtArray[$tgtKey] = $tgtVal;
                    }
                }
            }
        }
    }
    return $tgtArray;
}

// Rearrange Source Array according to the Matched Field Headers in merge page
function rearrangeArray($sourceArray, $index, $targetHeader, $newSourceHeader) {
    $newSourceEntry = array_values($sourceArray)[$index];
    $tempArray = [];

    foreach ($targetHeader as $key => $targetField) {
        if ($newSourceHeader[$key] == '<blank>') {
            $tempArray[$targetField] = '';
        } else {
            $tempArray[$targetField] = $newSourceEntry[$newSourceHeader[$key]];
        }
    }
    return $tempArray;
}

// Converts the uploaded CSV files into arrays
function csvToArray($file) {
    $rows = array();
    $headers = array();
    if (file_exists($file) && is_readable($file)) {
        $handle = fopen($file, 'r');
        while (!feof($handle)) {
            $row = fgetcsv($handle, 10240, ',', '"');
            if (empty($headers))
                $headers = $row;
            else if (is_array($row)) {
                array_splice($row, count($headers));
                $rows[] = array_combine($headers, $row);
            }
        }
        fclose($handle);
    } else {
        // Redirect to upload page if file is corrupted or not uploaded
        $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/upload.php";
        header("Location: " . $redirectUrl);
        die();
    }
    return $rows;
}

// Converts merged array into downloadable CSV file
function arrayToCsv(array &$array) {
    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen("php://output", 'w');
    fputcsv($df, array_keys(reset($array)));
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}

// Initialise download header
function sendDownloadHeader($filename) {
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 01 Jan 2111 11:11:11 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

// Send download header to client, downloading the generated CSV file to
// client's computer
function downloadCsv($array) {
    sendDownloadHeader("[" . date("Y.m.d") . "]_Merged_Data_Export.csv");
    echo arrayToCsv($array);
}

?>
