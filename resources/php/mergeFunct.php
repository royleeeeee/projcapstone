<?php

include '../resources/php/function.php';

session_start();

// Redirect to upload.php if merge.php is accessed before uploading the CSV files
if (!isset($_SESSION['sourceArray']) && !isset($_SESSION['targetArray'])) {
    $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/upload.php";
    header("Location: ". $redirectUrl);
    die();
}

else {
    // Initialise error message variable
    $errNoKey = false;

    // When the "Merge & Download" Buttom is clicked
    if (isset ($_POST['mergeButton'])) {

        // Show error message if either Primary Key or Foreign Key is not selected
        if (!isset($_POST['sourcePK']) || !isset($_POST['targetPK']) ) {
            $errNoKey = true;
        }

        else {
            // Assign Primary Key and Foreign Key as cookies
            $_SESSION['sourcePK'] = $_POST['sourcePK'];
            $_SESSION['targetPK'] = $_POST['targetPK'];

            // Match both Source and Target Array
            $outputArray = matchArray($_SESSION['sourceArray'], $_SESSION['targetArray'],
                                $_SESSION['sourcePK'], $_SESSION['targetPK'],
                                $_SESSION['targetHeader'], $_POST['newSourceHeader']);

            // Convert merged array to CSV file and send download header to client
            downloadCsv($outputArray);
            die();
        }
    }
}



?>