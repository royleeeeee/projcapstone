<?php

include '../resources/php/function.php';

// Clean up all cookies when the page loads
session_start();
session_destroy();

// Initialise error message variable
$errNoFile = false;
$errNoCSV = false;

// When the "Upload" Button is pressed
if (isset ($_POST['uploadButton'])) {

    // Show error message if either Target or Source CSV file is not uploaded
    if (empty($_FILES['csvSource']['tmp_name']) || empty($_FILES['csvTarget']['tmp_name'])) {
        $errNoFile = true;
    }

    else {
        // Show error message if the files uploaded does not have a .csv extension
        $srcFName = $_FILES['csvSource']['name'];
        $tgtFName = $_FILES['csvTarget']['name'];
        if (!(substr_compare($srcFName, ".csv", strlen($srcFName)-strlen(".csv"),
                    strlen(".csv")) === 0)
            || !(substr_compare($tgtFName, ".csv", strlen($tgtFName)-strlen(".csv"),
                    strlen(".csv")) === 0)) {
            $errNoCSV = true;
        }

        else {
            session_start();

            // Get the uploaded CSV files and convert them to array
            // then assign the arrays as cookies
            $sourceArray = csvToArray($_FILES['csvSource']['tmp_name']);
            $targetArray = csvToArray($_FILES['csvTarget']['tmp_name']);
            $_SESSION['sourceArray'] = $sourceArray;
            $_SESSION['targetArray'] = $targetArray;

            // Get the first row of both arrays as field headers
            // then assign them as cookies
            $sourceHeader = array_keys($sourceArray[0]);
            $targetHeader = array_keys($targetArray[0]);
            $_SESSION['sourceHeader'] = $sourceHeader;
            $_SESSION['targetHeader'] = $targetHeader;

            // Get the name of the uploaded CSV files and assign them as cookies
            $sourceFileName = $_FILES['csvSource']['name'];
            $targetFileName = $_FILES['csvTarget']['name'];
            $_SESSION['sourceFileName'] = $sourceFileName;
            $_SESSION['targetFileName'] = $targetFileName;

            // Redirect to merge.php
            $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/merge.php";
            header("Location: ". $redirectUrl);
            die();
        }
    }


}

?>