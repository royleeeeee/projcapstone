<?php include '../resources/php/mergeFunct.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Merge Database</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="author" content="Jonathan Zheng, Vaibhav Sharma, Kloe Wade, Roy Lee">
    <meta name="description" content="2018 Capstone Project by Team Fantastic Four">

    <link rel="stylesheet" href="../resources/stylesheet/style.css"/>
    <link rel="shortcut icon" type="image/png" href="../resources/image/favicon.png"/>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    <script src="../resources/javascript/javascript.js"></script>

</head>

<body class='wrapperBody'>

    <!-- Banner and Navigation Bar -->
    <?php
        $page = 'merge';
        include 'include/navbar.php';
    ?>

    <div class='wrapperContent'>

        <article class="content">

            <!-- Page Title -->
            <header><h2>Select Identifier Keys and Match Field Headers</h2></header>

            <!-- Display error messag if Primary and Foreign Key are not selected -->
            <span id="errNoKey" class ="errorMessage">
                A Primary Key and Foreign Key must be selected.
            </span>
            <?php if($errNoKey) {
                echo '<script>displayElement("errNoKey");</script>';
            } ?>

            <!-- Rearranges Source Field Headers then Merge it with Target CSV -->
            <form method="post" action="#">

                <table class="headerTable">
                <?php
                    echo '                        
                        <tr>
                            <th>Target Database ('. $_SESSION['targetFileName'] .') Primary Key</th>
                            <th>Source Database ('. $_SESSION['sourceFileName'] .') Foreign Key</th>
                        </tr>
                        
                        <tr class="headerRow">
                            <td><select class="headerSelect" name="targetPK" 
                                        onclick="hideElement(\'errNoKey\'); hideElement(\'errNoKey2\')">           
                                <option value="" disabled selected>Select a Primary Key</option>
                    ';

                    // List Target Field Headers as Primary Option
                    foreach ($_SESSION['targetHeader'] as $targetValue) {
                        echo '<option value=\'' . $targetValue . '\'>' . $targetValue . '</option>';
                    }

                    echo '
                            </select></td>
                            
                            <td><select class="headerSelect" name="sourcePK" 
                                        onclick="hideElement(\'errNoKey\'); hideElement(\'errNoKey2\')">">  
                                <option value="" disabled selected>Select a Foreign Key</option>                          
                    ';

                    // List Source Field Headers as Foreign Key Option
                    foreach ($_SESSION['sourceHeader'] as $sourceValue) {
                        echo '
                            <option value=\'' . $sourceValue . '\'>' . $sourceValue . '</option>
                        ';
                    }

                    echo '
                            </select></td>                            
                        </tr>
                        
                        <tr>
                            <th>Target Database ('. $_SESSION['targetFileName'] .') Field Headers</th>
                            <th>Source Database ('. $_SESSION['sourceFileName'] .') Field Headers</th>
                        </tr>
                    ';

                    // List out both Source and Target Field Headers to, allowing the user to match them.
                    foreach ($_SESSION['targetHeader'] as $targetValue) {
                        echo '
                            <tr class="headerRow">
                                <td >' . $targetValue . '</td>
                                <td><select class="headerSelect" name="newSourceHeader[]">
                                    <option value="<blank>">&lt;blank&gt;</option>
                        ';

                        foreach ($_SESSION['sourceHeader'] as $sourceValue) {
                            echo ' 
                                    <option value=\'' . $sourceValue . '\'>' . $sourceValue . '</option>
                            ';
                        }

                        echo '
                                </select></td>
                            </tr>
                        ';
                    }
                ?>
                </table>

                <!-- Show a warning detailing the default conflict resolution behavior -->
                <div class="mergeWarning">
                    <b>WARNING</b>
                    <br>
                    If the same entry is to exist in both Target and Source Database,
                    any differing values from the Target Database will be replaced by
                    values from the same Field in Source Database. To avoid making changes
                    to a Field in Target Database, select <span style="color:#22448B">&lt;blank&gt;</span>
                    as its matching Source Field Header. It is strongly advised that a backup
                    of the existing database is made and a throughout scrutiny is carried out
                    on the files generated by this application before it is imported into
                    existing database systems.
                    <br><br>
                    <b>The development team will not be responsible for any loss of data
                    caused by the use of this application.</b>
                </div>

                <!-- Enables Merge Button when the checkbox is clicked -->
                <div>
                    <label class="acceptCondtLabel">
                        <input type="checkbox"  class="acceptCondtCheckBox"
                           onchange="document.getElementById('mergeButton').disabled = !this.checked;"/>
                        I have read the warning and agree with the statements.
                    </label>
                </div>

                <!-- Display error messag if Primary and Foreign Key are not selected -->
                <span id="errNoKey2" class ="errorMessage">
                    A Primary Key and Foreign Key must be selected.
                </span>
                <?php if($errNoKey) {
                    echo '<script>displayElement("errNoKey2");</script>';
                } ?>

                <!-- Submit the rearranged Source Field Header to mergeFunct.php -->
                <input id="mergeButton" type='submit' name='mergeButton' value='Merge & Download' disabled>

            </form>

        </article>

    </div>

    <!-- Footer -->
    <?php include 'include/footer.php'; ?>

</body>

</html>