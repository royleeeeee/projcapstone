<?php include '../resources/php/indexFunct.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Home</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="author" content="Jonathan Zheng, Vaibhav Sharma, Kloe Wade, Roy Lee">
    <meta name="description" content="2018 Capstone Project by Team Fantastic Four">

    <link rel="stylesheet" href="../resources/stylesheet/style.css"/>
    <link rel="shortcut icon" type="image/png" href="../resources/image/favicon.png"/>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    <script src="../resources/javascript/javascript.js"></script>

</head>

<body class='wrapperBody'>

    <!-- Banner and Navigation Bar -->
    <?php
        $page = 'index';
        include 'include/navbar.php';
    ?>

    <div class='wrapperContent'>

        <article class="content">

            <!-- Page name -->
            <header><h2>Welcome</h2></header>

			<p class="indexParagraph">
				This is a database merging tool and is designed to upload player data from
                two different sources and merge them to fit the layout currently used by
                Capalaba Bulldogs Football Club. The data used is confidential and contains
                personal information of players.
                <br>
                <br>
                Integrated into the application is a live and updated social media aspect; this
                includes personal profiles of Capalaba and recently published posts. The user may
                access their details via Quick Access links or stay up to date with their latest
                posts in one central location.
			</p>

		</article>
    </div>

    <!-- Footer -->
    <?php include 'include/footer.php'; ?>

</body>

</html>