<?php include '../resources/php/socialmediaFunct.php'; ?>

<!DOCTYPE html>
<html lang="en" class="htmlBody">

<head>

    <title>Social Media</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="author" content="Jonathan Zheng, Vaibhav Sharma, Kloe Wade, Roy Lee">
    <meta name="description" content="QUT Capstone Project for Capalab Bulldogs Football Club">

    <link rel="stylesheet" href="../resources/stylesheet/styleSM.css"/>
    <link rel="shortcut icon" type="image/png" href="../resources/image/favicon.png"/>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" >
    <script src="../resources/javascript/javascript.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 

</head>

<body class='wrapperBody'>
    <!-- Banner and Navigation Bar -->
    <?php
        $page = 'socialMedia';
        include 'include/navbar.php';
    ?>

<div class='wrapperContent'>
	<!-- New section for the Social Media Quick Links-->
	<article class="content">
            <!-- Page name -->
            <header><h2 style="
			color: #23448c; 
			font-family: Lato, sans-serif;
			margin-top: 0;
		    display: block;
			font-size: 24px;
			font-weight: 700;
			font-weight: bold;"> Capalaba Bulldogs Social Media </h2></header>
			<p> Quick access: </p>
				<a href="https://www.facebook.com/capalabafc/" target=”_blank” class="fa fa-facebook" style="text-decoration: none;"></a>
				<a href="https://twitter.com/capalaba_fc" target=”_blank” class="fa fa-twitter" style="text-decoration: none;"></a>
				<a href="https://www.instagram.com/capalababulldogs/" target=”_blank” class="fa fa-instagram" style="background: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%,#d6249f 60%,#285AEB 90%);; text-decoration: none;" ></a>
			<br> 
	</article> 
			
<!-- New section for the Facebook Section -->
<article class="content" style="width: 100%;">
	<h3> Facebook Page </h3>
	<!-- This will directly link to the users Facebook messenger inbox -->
		<a href="https://m.me/capalababulldogs" style="text-decoration: none;">
				View and Reply to Facebook Messages
			</a>
			<br> 
			<br>
					<div class="fb-page" 
     data-href="https://www.facebook.com/capalabafc/?ref=br_rs"
     data-small-header="false"  
     data-hide-cover="false"    
     data-show-facepile="true"  
     data-show-posts="false">
</div>

<div id="fb-root"></div>
</article> 

<!-- New section for the Twitter Feed -->
<article class="content" style="background: lightgrey; width: 100%;">
	<h3> Twitter Live Feed </h3>
	<br> 
 <div class="container">
    <div class="col-lg-12"> 
	<?php 
		// keys and codes generated from personal Twitter account
		$oauth_access_token = "1048770121785782272-hlEV2nY0cQZGbAg2ZMfXRmeovlksNm";
		$oauth_access_token_secret = "6PUS3km2WJyjsneY01AwnY6xdl5BXwsOehKT33ggJNUXC";
		$consumer_key = "HxSo7BOJkJQ1LNdBTBnfenyKu";
		$consumer_secret = "Ap7dvmoONGE6EEehCQoXneh0zkkySM2YPg4nJQgH2MwFL5oUHP";
		 
		// we are going to use "user_timeline"
		$twitter_timeline = "user_timeline";
		 
		// specify number of tweets to be shown
		$request = array(
			'count' => '10'	);

		// put oauth values in one oauth array variable
		$oauth = array(
			'oauth_consumer_key' => $consumer_key,
			'oauth_nonce' => time(),
			'oauth_signature_method' => 'HMAC-SHA1',
			'oauth_token' => $oauth_access_token, 
			'oauth_timestamp' => time(),
			'oauth_version' => '1.0'
		);
		 
		// combine request and oauth in one array
		$oauth = array_merge($oauth, $request);
		 
		// make base string
		$baseURI="https://api.twitter.com/1.1/statuses/$twitter_timeline.json";
		$method="GET";
		$params=$oauth;
		 
		$r = array();
		ksort($params);
		foreach($params as $key=>$value){
			$r[] = "$key=" . rawurlencode($value);
		}
		$base_info = $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
		$composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
		 
		// get oauth signature
		$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
		$oauth['oauth_signature'] = $oauth_signature;

		// make request make auth header
		$r = 'Authorization: OAuth ';
			 
		$values = array();
		foreach($oauth as $key=>$value){
			$values[] = "$key=\"" . rawurlencode($value) . "\"";
		}
		$r .= implode(', ', $values);
		 
		// get auth header
		$header = array($r, 'Expect:');
		 
		// set cURL options
		$options = array(
			CURLOPT_HTTPHEADER => $header,
			CURLOPT_HEADER => false,
			CURLOPT_URL => "https://api.twitter.com/1.1/statuses/$twitter_timeline.json?". http_build_query($request),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true
		);

		// retrieve the twitter feed
		$feed = curl_init();
		curl_setopt_array($feed, $options);
		$json = curl_exec($feed);
		curl_close($feed);
		 
		// decode json format tweets
		$tweets=json_decode($json, true);
	?>
</div>
     
	 
<div class="col-lg-4">
	<?php        
		// show user information
		echo "<div class='overflow-hidden'>";
		 
		// user data
		$profile_photo=str_replace("normal", "400x400", $tweets[0]['user']['profile_image_url_https']);
		$name=$tweets[0]['user']['name'];
		$screen_name=$tweets[0]['user']['screen_name'];
		 
		// show profile photo
		echo "<img src='{$profile_photo}' class='img-thumbnail' />";
		 
		// show other information about the user
		echo "<div class='text-align-center'>";
			echo "<div><h2>{$name}</h2></div>";
			echo "<div><a href='https://twitter.com/{$screen_name}' target='_blank'>@{$screen_name}</a></div>";
		echo "</div>";
		 
	echo "</div>";
	?>

</div> <!-- end <div class="col-lg-4"> -->
 
<div class="col-lg-8">
	<?php
		// show the tweets and followers count 
		$statuses_count=$tweets[0]['user']['statuses_count'];
		$followers_count=$tweets[0]['user']['followers_count'];
		 
		// show numbers
		echo "<div class='overflow-hidden'>";
		 
		// show number of tweets
		echo "<div class='float-left margin-right-2em text-align-center'>";
			echo "<div class='color-black'>Tweets</div>";
			echo "<div class='badge font-size-20px'>" . number_format($statuses_count, 0, '.', ',') . "</div>";
		echo "</div>";
		 
		// show number of followers
		echo "<div class='float-left margin-right-2em text-align-center'>";
			echo "<div class='color-black'>Followers</div>";
			echo "<div class='badge font-size-20px'>" . number_format($followers_count, 0, '.', ',') . "</div>";
		echo "</div>";
		 
	echo "</div>";
	 

	// show tweets
    if($tweets) {
    foreach ($tweets as $tweet) {
        // show a tweet
        echo "<div class='overflow-hidden'>";
        // show tweet content
        echo "<div class='tweet-text'>";
        // show name and screen name
        echo "<h4 class='margin-top-4px'>";
        echo "<a href='https://twitter.com/{$screen_name}'>{$name}</a> ";
        echo "<span class='color-white'>@{$screen_name}</span>";
        echo "</h4>";
        // show tweet text
        echo "<div class='margin-zero'>";
        // get tweet text
        $tweet_text = $tweet['text'];
        // make links clickable
        $tweet_text = preg_replace('@(https?://([-\w\.]+)+(/([\w/_\.]*(\?\S+)?(#\S+)?)?)?)@', '<a href="$1" target="_blank">$1</a>', $tweet_text);
        // output
        echo $tweet_text;
        echo "</div>";
        echo "</div>";
        echo "</div>";
    }
    }
	?>
     </div> <!-- end <div class="col-lg-8"> -->
	
</div> <!-- end <div class="container"> -->
</article>	


<!-- New section for the Instagram Feed -->
	<article class="content">
		<h3> Instagram Live Feed </h3>
	</article>

	<?php
			$access_token="9008269068.1677ed0.f743f1689c264e269233ddadaf0729e1";
			$photo_count=9;
				 
			$json_link="https://api.instagram.com/v1/users/self/media/recent/?";
			$json_link.="access_token={$access_token}&count={$photo_count}";
			
			$json = file_get_contents($json_link);
			$obj = json_decode(preg_replace('/("\w+"):(\d+)/', '\\1:"\\2"', $json), true);
			
		foreach ($obj['data'] as $post) {
		
		// show the caption in text
		$pic_text=$post['caption']['text'];
		// show the posts and have a links available to front end users
		$pic_link=$post['link'];
		// show the likes and counting 
		$pic_like_count=$post['likes']['count'];
		// show the comment counts 
		$pic_comment_count=$post['comments']['count'];
		$pic_src=str_replace("http://", "https://", $post['images']['standard_resolution']['url']);
		// the caption and time of image is displayed 
		$pic_created_time=date("F j, Y", $post['caption']['created_time']);
		$pic_created_time=date("F j, Y", strtotime($pic_created_time . " +1 days"));
		 
		echo "<div class='col-md-4 col-sm-6 col-xs-12 item_box'>";        
			echo "<a href='{$pic_link}' target='_blank'>";
				echo "<img class='img-responsive photo-thumb' src='{$pic_src}' alt='{$pic_text}'>";
			echo "</a>";
			echo "<p>";
				echo "<p>";
					echo "<div style='color:#888;'>";
						echo "<a href='{$pic_link}' target='_blank'>{$pic_created_time}</a>";
					echo "</div>";
				echo "</p>";
				echo "<p>{$pic_text}</p>";
			echo "</p>";
		echo "</div>";
		}
	?>

    </div>

    <!-- Footer -->
    <?php include 'include/footer.php'; ?>
	</body>
</html>

