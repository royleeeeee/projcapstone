<header class='wrapperHeader'>

    <!-- Add the link to "active" class when its page is selected -->
    <nav><ul>

        <li class='<?php echo (($page=='index') ? 'active':''); ?>'>
            <a href='index.php' id='navbarHome'>ProjectCapstone</a>
        </li>

        <li class='<?php echo (($page=='merge') ? 'active':''); ?>'>
            <a href='upload.php'>Merge CSV</a>
        </li>

        <li class='<?php echo (($page=='socialMedia') ? 'active':''); ?>'>
            <a href='socialmedia.php'>Social Media</a>
        </li>

    </ul></nav>

</header>
