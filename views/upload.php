<?php include '../resources/php/uploadFunct.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Upload Files</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="author" content="Jonathan Zheng, Vaibhav Sharma, Kloe Wade, Roy Lee">
    <meta name="description" content="2018 Capstone Project by Team Fantastic Four">

    <link rel="stylesheet" href="../resources/stylesheet/style.css"/>
    <link rel="shortcut icon" type="image/png" href="../resources/image/favicon.png"/>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    <script src="../resources/javascript/javascript.js"></script>

</head>

<body class='wrapperBody'>

    <!-- Banner and Navigation Bar -->
    <?php
        $page = 'merge';
        include 'include/navbar.php';
    ?>

    <div class='wrapperContent'>

        <article class="content">

            <!-- Page Title -->
            <header><h2>Upload CSV Files</h2></header>

            <!-- Browse for CSV Files and submit them to uploadFunct.php -->
            <form enctype="multipart/form-data" action="#" method="post" id="uploadForm">

                <table>

                    <!-- Browse and Select Source CSV File -->
                    <tr><td>
                        <label for="csvSource">Select <b>Source</b> CSV File</label>
                        <input type="hidden" name="MAX_FILE_SIZE" value="10000000"/>
                        <input type="file" name="csvSource" id="csvSource" value=""
                               onclick="hideElement('errNoFile'); hideElement('errNoCSV')"/>
                    </td></tr>

                    <!-- Browse and Select Target CSV File -->
                    <tr><td>
                        <label for="csvTarget">Select <b>Target</b> CSV File</label>
                        <input type="hidden" name="MAX_FILE_SIZE" value="10000000"/>
                        <input type="file" name="csvTarget" id="csvTarget" value=""
                               onclick="hideElement('errNoFile'); hideElement('errNoCSV')"/>
                    </td></tr>

                    <!-- Submit Source and Target files to merge.csv -->
                    <tr><td>
                        <!-- Display error messag if Target and Source Files are not uploaded -->
                        <span id="errNoFile" class ="errorMessage">
                            A Source File and Target File must be selected.
                        </span>
                        <?php if($errNoFile) {
                            echo '<script>displayElement("errNoFile");</script>';
                        } ?>

                        <!-- Display error message if the files does not have .csv extension -->
                        <span id="errNoCSV" class ="errorMessage">
                            Both Source Files and Target Files must have .csv extension
                        </span>
                        <?php if($errNoCSV) {
                            echo '<script>displayElement("errNoCSV");</script>';
                        } ?>

                        <input type="submit" name="uploadButton" value="Upload"/>
                    </td></tr>

                </table>

            </form>

        </article>

    </div>

    <!-- Footer -->
    <?php include 'include/footer.php'; ?>

</body>

</html>